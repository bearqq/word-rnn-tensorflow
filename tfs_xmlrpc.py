# coding: utf-8
# python3
#some definitions:

#imports:

#tensorflow
import os
import numpy as np
import tensorflow as tf
from six.moves import cPickle
from utils import TextLoader
from model import Model
from six import text_type

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

save_dir='save'
n=50
sample=1
stop=1
temperature=0.1
text=None

with open(os.path.join(save_dir, 'config.pkl'), 'rb') as f:
    saved_args = cPickle.load(f)
with open(os.path.join(save_dir, 'chars_vocab.pkl'), 'rb') as f:
    chars, vocab = cPickle.load(f)

def rep_g():
    with tf.Session() as sess:
        modelx = Model(saved_args, True)
        tf.initialize_all_variables().run()
        saver = tf.train.Saver(tf.all_variables())
        ckpt = tf.train.get_checkpoint_state(save_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
        #modelx.save_para(sess, chars, vocab, n, sample, stop)
        
        while 1:
            try:
                r0=modelx.sample(sess, chars, vocab, n, text, sample, temperature=temperature, stop=2)
                #print(r.encode('utf-8'))
                yield r0
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)
            """
            prime=text if text.endswith('\n') else text+'\n'
            try:
                r0=modelx.sample(sess, chars, vocab, n, text, sample, temperature=temperature, stop=1)
                #print(r.encode('utf-8'))
                #yield r
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)
            
            
            try:
                r=modelx.sample(sess, chars, vocab, n, prime, sample, temperature=temperature, stop=1)
                #print(r.encode('utf-8'))
                yield r0+'\r\n'+r.split('\n')[1]
            except Exception as e:
                logger.info(e)
                yield 'Error input'+str(e)"""
rep=rep_g()


from xmlrpc.server import SimpleXMLRPCServer
def reply(s):
    global text
    text=s.replace('\r','')
    r=next(rep)
    return r

serv = SimpleXMLRPCServer(('', 15000))
serv.register_function(reply)
serv.serve_forever()






